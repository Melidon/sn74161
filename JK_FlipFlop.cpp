#include "JK_FlipFlop.h"

void JK_FlipFlop::JK_FlipFlop_main()
{
	if (CLR.read())
	{
		Q.write(0);
		Q_n.write(1);
	}

	else if (J.read() != K.read())
	{
		Q.write(J.read());
		Q_n.write(K.read());
	}

	else if (J.read() && K.read())
	{
		Q.write(!Q.read());
		Q_n.write(!Q_n.read());
	}

	else
	{
		Q.write(Q.read());
		Q_n.write(Q_n.read());
	}
}
