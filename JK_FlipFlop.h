
#pragma once
#include "systemc.h"

SC_MODULE(JK_FlipFlop)
{
	sc_in<bool> J;
	sc_in<bool> K;
	sc_in<bool> CK;
	sc_in<bool> CLR;

	sc_out<bool> Q;
	sc_out<bool> Q_n;

	void JK_FlipFlop_main();

	SC_CTOR(JK_FlipFlop)
	{
		SC_METHOD(JK_FlipFlop_main);
		sensitive << CLR << CK;
		Q.initialize(0);
		Q_n.initialize(1);
	}
};
