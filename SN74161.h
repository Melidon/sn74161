#pragma once
#include "systemc.h"
#include "JK_FlipFlop.h"

SC_MODULE(SN74161)
{
    sc_in<bool> LOAD_n;
    sc_in<bool> DATA_A;
    sc_in<bool> DATA_B;
    sc_in<bool> CLK;
    sc_in<bool> DATA_C;
    sc_in<bool> DATA_D;
    sc_in<bool> CLR_n;
    sc_in<bool> ENP;
    sc_in<bool> ENT;

    sc_out<bool> Q_A;
    sc_out<bool> Q_B;
    sc_out<bool> Q_C;
    sc_out<bool> Q_D;
    sc_out<bool> Q_RCO;

    void SN74161_main();

    SC_CTOR(SN74161)
    {
        SC_METHOD(SN74161_main);
        sensitive;
    }
};
